from requests import put, get, delete
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
from os import listdir
main_url = 'https://www.openstreetmap.org'
responses = []


def setup_module(module):
    data = str()
    for line in open('test_changesets/test1.xml', 'r'):
        data += line
    response = put(
        main_url + '/api/0.6/changeset/create',
        headers={'Content-Type': 'text/xml'},
        auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
        data=data
    )
    for file in listdir('test_nodes'):
        xml = ET.parse('test_nodes/{}'.format(file))
        nodes = xml.findall('node')
        for node in nodes:
            node.attrib['changeset'] = response.text
        xml.write('test_nodes/{}'.format(file))


def teardown_module(module):
    xml = ET.parse('test_nodes/node1.xml')
    node = xml.find('node')
    response = put(
        main_url + '/api/0.6/changeset/{}/close'.format(node.get('changeset')),
        auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs')
    )


def read_xml(file):
    data = str()
    for line in open('test_nodes/{}'.format(file), 'r'):
        data += line
    return data


def test_create_element():
    files = listdir('test_nodes')
    for file in files:
        if file.startswith('node'):
            data = read_xml(file)
            response = put(
                main_url + '/api/0.6/node/create',
                headers={'Content-Type': 'text/xml'},
                auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
                data=data
            )
            assert response.status_code == 200
            assert response.reason == 'OK'
            assert response.text.isdigit()
            responses.append(response)


def test_update_element():
    files = listdir('test_nodes')
    for create_response in responses:
        for file in files:
            if file.startswith('updated'):
                xml = ET.parse('test_nodes/{}'.format(file))
                nodes = xml.findall('node')
                for node in nodes:
                    node.attrib['id'] = create_response.text
                    node.attrib['version'] = '1'
                xml.write('test_nodes/{}'.format(file))
                data = read_xml(file)
                response = put(
                    main_url + '/api/0.6/node/{}'.format(create_response.text),
                    headers={'Content-Type': 'text/xml'},
                    auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
                    data=data
                )
                assert response.status_code == 200
                assert response.reason == 'OK'
                assert response.text.isdigit()
                assert response.text == '2'


def test_delete_element():
    files = listdir('test_nodes')
    for create_response in responses:
        for file in files:
            if file.startswith('updated'):
                xml = ET.parse('test_nodes/{}'.format(file))
                nodes = xml.findall('node')
                for node in nodes:
                    node.attrib['id'] = create_response.text
                    node.attrib['version'] = '2'
                xml.write('test_nodes/{}'.format(file))
                data = read_xml(file)
                response = delete(
                    main_url + '/api/0.6/node/{}'.format(create_response.text),
                    headers={'Content-Type': 'text/xml'},
                    auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
                    data=data
                )
                assert response.status_code == 200
                assert response.reason == 'OK'
                assert response.text.isdigit()
                assert response.text == '3'


def test_get_history_element():
    for create_response in responses:
        response = get(
            main_url + '/api/0.6/node/{}/history'.format(create_response.text),
            headers={'Content-Type': 'text/xml'},
            auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
        )
        assert response.status_code == 200
        assert response.reason == 'OK'
        xml = ET.fromstring(response.text)
        nodes = xml.findall('node')
        assert len(nodes) == 3


def test_get_version_element():
    for create_response in responses:
        response = get(
            main_url + '/api/0.6/node/{}/3'.format(create_response.text),
            headers={'Content-Type': 'text/xml'},
            auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
        )
        assert response.status_code == 200
        assert response.reason == 'OK'
        xml = ET.fromstring(response.text)
        node = xml.find('node')
        assert node.get('id') == create_response.text
