from requests import put, get
from requests.auth import HTTPBasicAuth
import xml.etree.ElementTree as ET
from os import listdir
main_url = 'https://www.openstreetmap.org'
responses = []


def check_xml_identity(original_file, received_file):
    original_xml = ET.parse('test_changesets/{}'.format(original_file))
    received_xml = ET.fromstring(received_file)
    original_xml_tags = original_xml.findall('*/tag')
    received_xml_tags = received_xml.findall('*/tag')
    assert len(original_xml_tags) == len(received_xml_tags)
    for i in range(len(original_xml_tags)):
        assert original_xml_tags[i].get('k') == received_xml_tags[len(received_xml_tags) - 1 - i].get('k')
        assert original_xml_tags[i].get('v') == received_xml_tags[len(received_xml_tags) - 1 - i].get('v')


def read_xml(file):
    data = str()
    for line in open('test_changesets/{}'.format(file), 'r'):
        data += line
    return data


def test_changeset_create():
    files = listdir('test_changesets')
    for file in files:
        if file.startswith('test'):
            data = read_xml(file)
            response = put(
                main_url + '/api/0.6/changeset/create',
                headers={'Content-Type': 'text/xml'},
                auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
                data=data
            )
            assert response.status_code == 200
            assert response.reason == 'OK'
            assert response.text.isdigit()
            responses.append(response)


def test_changeset_get():
    files = listdir('test_changesets')
    for create_response in responses:
        response = get(
            main_url + '/api/0.6/changeset/{}'.format(create_response.text)
        )
        assert response.status_code == 200
        assert response.reason == 'OK'
        for file in files:
            if file.startswith('test'):
                check_xml_identity(file, response.text)
                files.remove(file)
                break


def test_changeset_update():
    files = listdir('test_changesets')
    for create_response in responses:
        for file in files:
            if file.startswith('updated'):
                data = read_xml(file)
                response = put(
                    main_url + '/api/0.6/changeset/{}'.format(create_response.text),
                    headers={'Content-Type': 'text/xml'},
                    auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs'),
                    data=data
                )
                assert response.status_code == 200
                assert response.reason == 'OK'
                check_xml_identity(file, response.text)
                files.remove(file)
                break


def test_changeset_close():
    for create_response in responses:
        response = put(
            main_url + '/api/0.6/changeset/{}/close'.format(create_response.text),
            auth=HTTPBasicAuth('fishenko.vlad@mail.ru', '9^`^$D+,aj+beMs')
        )
        assert response.status_code == 200
        assert response.reason == 'OK'
        assert response.text == ''
