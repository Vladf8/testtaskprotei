*** Settings ***
Documentation    Check current api version
Library           RequestsLibrary
Library           XML

*** Variables ***
${api version}      0.6

*** Test Cases ***
Get Request Test
    Create Session    openstreetmap             https://www.openstreetmap.org/
    ${resp_openstreetmap}=   GET On Session     openstreetmap             /api/capabilities           expected_status=200
    Should Be Equal As Strings          ${resp_openstreetmap.reason}    OK
    ${xml}=     parse xml       ${resp_openstreetmap.text}
    ${recieved api version}=     get element attribute       ${xml}      version
    should be equal     ${recieved api version}     ${api version}
