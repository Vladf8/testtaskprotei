from requests import get
import xml.etree.ElementTree as ET

main_url = 'https://www.openstreetmap.org'
response = get(main_url+'/api/capabilities')
xml = ET.fromstring(response.text)
expected_version = '0.6'


def test_expected_api_version():
    received_version = xml.get('version')
    assert response.status_code == 200
    assert received_version == expected_version


def test_latest_api_version():
    latest_version = xml.find('*/version').get('maximum')
    assert latest_version == expected_version
