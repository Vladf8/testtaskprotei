from requests import get
import xml.etree.ElementTree as ET


def test_bounding_box():
    main_url = 'https://www.openstreetmap.org'
    data = {
        '1': {'left': '30.181082', 'bottom': '59.860763', 'right': '30.181511', 'top': '59.861156',
              'result': {'1887613364': {'bench': 'yes', 'bus': 'yes', 'highway': 'bus_stop','local_ref': '2;2а;300',
                                        'name': 'Ленинский проспект, 72', 'passenger_information_display': 'no',
                                        'public_transport': 'platform', 'shelter': 'yes', 'surface': 'asphalt',
                                        'tactile_paving': 'no'}}},
        '2': {'left': '30.313235', 'bottom': '59.938918', 'right': '30.313960', 'top': '59.941733',
              'result': {'1419724993': {'bench': 'yes', 'highway': 'bus_stop', 'name': 'Дворцовая площадь',
                                        'name:ru': 'Дворцовая площадь', 'public_transport': 'platform',
                                        'shelter': 'yes', 'tactile_paving': 'no', 'trolleybus': 'yes'}}}
    }
    for key in data:
        test_box = data[key]
        response = get(main_url + '/api/0.6/map?bbox={},{},{},{}'.format(test_box['left'], test_box['bottom'],
                                                                         test_box['right'], test_box['top']))
        xml = ET.fromstring(response.text)
        for node in xml.findall('node'):
            if node.get('id') in test_box['result'].keys():
                for tag in node.findall('tag'):
                    if tag.get('k') in test_box['result'][node.get('id')].keys():
                        assert tag.get('v') == test_box['result'][node.get('id')][tag.get('k')]

